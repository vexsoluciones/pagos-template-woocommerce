<?php

function check_plugin_update() {
    $manifest_url = 'https://www.pasarelasdepagos.com/plugins/vex-uber-direct/plugin-manifest.json';
    $response = wp_remote_get( $manifest_url );

    if( is_wp_error( $response ) ) {
        return;
    }

    $body = wp_remote_retrieve_body( $response );
    $data = json_decode( $body );

    if( !isset( $data->version ) || !isset( $data->download_url ) ) {
        return;
    }

    $current_version = get_option( 'template-payment_version', '0' ); // Obtén la versión actual de tu plugin

    //$current_version = '1.1.0'; // Obtén la versión actual de tu plugin
    $latest_version = $data->version;

    if( version_compare( $latest_version, $current_version, '>' ) ) {
        add_action( 'admin_notices', 'display_update_notice' );
    }
}
add_action( 'admin_init', 'check_plugin_update' );

function display_update_notice() {
    $activated = get_option( 'woocommerce_template-payment_activated', '0' );
    ?>
    <div class="notice notice-info is-dismissible">

        <p>
            <?php _e( 'There is a new version of Template payment available..', 'template-payment' ); ?>

            <?php
                if ($activated == 1){
            ?>
                    <button id="plugin-update-button" class="button"><?=__('Update Now', 'template-payment')?></button>
            <?php
                }
            ?>
        </p>
    </div>
    <?php
}



function download_and_install_plugin() {

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        ob_start();
        // Realiza la lógica de actualización del plugin aquí
        $manifest_url = 'https://www.pasarelasdepagos.com/plugins/template-payment/plugin-manifest.json';
        $response = wp_safe_remote_get($manifest_url);

        if (is_wp_error($response)) {
            return;
        }

        $body = wp_remote_retrieve_body($response);
        $data = json_decode($body);

        if (! isset($data->version) || ! isset($data->download_url)) {
            return;
        }

        $download_url = $data->download_url;
        $plugin_dir = plugin_dir_path(dirname(__FILE__));

        require_once(ABSPATH.'wp-admin/includes/file.php');
        require_once(ABSPATH.'wp-admin/includes/plugin.php');
        require_once(ABSPATH.'wp-admin/includes/misc.php');
        require_once(ABSPATH.'wp-admin/includes/class-wp-upgrader.php');

        rrmdir($plugin_dir);

        $upgrader = new Plugin_Upgrader();
        $result = $upgrader->install($download_url);

        if (is_wp_error($result)) {
            return;
        }

        $new_version = $data->version;
        update_option(PLUGIN_ID.'_version', $new_version); // Guardar la nueva versión en una opción
        // Redirecciona a la página de plugins después de la actualización
        //ob_clean();
        //header('Location: '.admin_url('plugins.php'));
        //wp_safe_redirect(admin_url('plugins.php'));
        if (ob_get_contents()) ob_end_clean();
        $redirect = admin_url('plugins.php');

        //wp_redirect($redirect);
        exit;
    }
}

add_action( 'wp_ajax_plugin_update', 'download_and_install_plugin' );


function enqueue_plugin_scripts() {
    wp_enqueue_script( 'plugin-update', plugin_dir_url( dirname(__FILE__) ) . 'assets/admin/js/plugin-update.js', array( 'jquery' ), '1.0', true );
    wp_localize_script( 'plugin-update', 'pluginData', array(
        'adminUrl' => admin_url('plugins.php')
    ));
}
add_action( 'admin_enqueue_scripts', 'enqueue_plugin_scripts' );

function rrmdir( $dir ) {

    if ( is_dir( $dir ) ) {

        $objects = scandir( $dir );
        foreach ( $objects as $object ) {
            if ( $object != '.' && $object != '..' ) {
                if ( is_dir( $dir . '/' . $object ) ) {
                    rrmdir( $dir . '/' . $object );
                } else {
                    unlink( $dir . '/' . $object );
                }
            }
        }
        rmdir( $dir );
    }
}
