<?php


class Vex_License
{

    const VISANET_PE_LICENSE_SECRET_KEY = '587423b988e403.69821411';
    const VISANET_PE_LICENSE_SERVER_URL = 'https://www.pasarelasdepagos.com';
    const VISANET_PE_ITEM_REFERENCE = 'Vexsoluciones - WooCommerce Gateway';


    protected $plugin = null;

    public function __construct(WC_Payment_Gateway $plugin)
    {
        $this->plugin = $plugin;
        // $this->verify();
    }

    /**
     * Initiate our verify.
     *
     * @since  2.0.0
     */
    public function verify()
    {
        $license = get_option('woocommerce_' . $this->plugin->id . '_license', '0');

        if (empty($license)) {
            $activated = get_option('woocommerce_' . $this->plugin->id . '_activated', '0');

            $this->disabled();
            return;
        }

        $activated = get_option('woocommerce_' . $this->plugin->id . '_activated', '0');
        $last_date = get_option('woocommerce_' . $this->plugin->id . '_last_date', '');


        $registro_licencia = get_option('woocommerce_' . $this->plugin->id . '_registo_'.$license, '0');

        if(!$registro_licencia){
            $this->activate($license);
        }else{
            $current_date = date('d/m/Y');
            //echo $current_date." - ".$last_date;
            //die();
            if (empty($last_date) && $activated) {
                return;
            }

            if ($last_date != $current_date) {

                $this->_verify($license);
            }

        }

    }

    private function disabled()
    {
        $settings = get_option('woocommerce_' . $this->plugin->id . '_settings', '');

        if (empty($settings)) return;

        $settings['enabled'] = 'no';

        update_option('woocommerce_' . $this->plugin->id . '_settings', $settings, 'no');
    }

    private function _verify($license)
    {

        $api_params = array(
            'slm_action'  => 'slm_check',
            'secret_key'  => self::VISANET_PE_LICENSE_SECRET_KEY,
            'license_key' => $license,
        );

        $url = add_query_arg($api_params, self::VISANET_PE_LICENSE_SERVER_URL);
        $args = array(
            'timeout'   => 20,
            'sslverify' => false
        );


        $response = wp_remote_get(esc_url_raw($url), $args);

        // Check the response code
        $response_code    = wp_remote_retrieve_response_code($response);
        $response_message = wp_remote_retrieve_response_message($response);

        if (200 != $response_code && !empty($response_message)) {
            WC_Admin_Settings::add_error('Vexsoluciones: ' . $response_code . ' - ' . $response_message);
            return;
        } elseif (200 != $response_code) {
            	update_option( 'woocommerce_' . $this->plugin->id . '_activated', '0', 'no' );
            // 	$this->disabled();

            WC_Admin_Settings::add_error('Vexsoluciones: Unexpected Error! The query returned with an error.');
            return;
        }

        $license_data = json_decode(wp_remote_retrieve_body($response));

        if ($license_data->result == 'success' && isset($license_data->status) && $license_data->status=='active') {

            if (strtotime($license_data->date_expiry) < strtotime(date('Y-m-d', time()))) {

                update_option( 'woocommerce_' . $this->plugin->id . '_activated', '0', 'no' );

            }else{
                $this->_activate_plugin($license_data->message);
            }



            return;
        }



        WC_Admin_Settings::add_error('Vexsoluciones: ' . $license_data->message);
        $this->disabled();
    }

    public function activate($license)
    {
        global $GLOBALS;



        $api_params = array(
            'slm_action'        => 'slm_activate',
            'secret_key'        => self::VISANET_PE_LICENSE_SECRET_KEY,
            'license_key'       => $license,
            'registered_domain' => $_SERVER['SERVER_NAME'],
            'item_reference'    => urlencode(self::VISANET_PE_ITEM_REFERENCE),
        );

        $query = esc_url_raw(add_query_arg($api_params, self::VISANET_PE_LICENSE_SERVER_URL));
        $response = wp_remote_get(esc_url_raw($query), array(
            'timeout'   => 20,
            'sslverify' => false
        ));

        // Check the response code
        $response_code    = wp_remote_retrieve_response_code($response);
        $response_message = wp_remote_retrieve_response_message($response);

        if (200 != $response_code && !empty($response_message)) {
            WC_Admin_Settings::add_error('Vexsoluciones: ' . $response_code . ' - ' . $response_message);
            return;
        } elseif (200 != $response_code) {
            WC_Admin_Settings::add_error('Vexsoluciones: Unexpected Error! The query returned with an error.');
            // $this->disabled();

            return;
        }

        $license_data = json_decode(wp_remote_retrieve_body($response));

        if ($license_data->result == 'success') {

            /*if (strtotime($license_data->date_expiry) < strtotime(date('Y-m-d', time()))) {
                return;
            }*/

            update_option('woocommerce_' . $this->plugin->id . '_registo_'.$license, '1', 'no');
            $this->_activate_plugin($license_data->message);
            return;
        }

        WC_Admin_Settings::add_error('Vexsoluciones: ' . $license_data->message);
        $this->disabled();
    }

    private function _activate_plugin($message)
    {
        update_option('woocommerce_' . $this->plugin->id . '_activated', '1', 'no');
        update_option('woocommerce_' . $this->plugin->id . '_last_date', date('d/m/Y'), 'no');

        WC_Admin_Settings::add_message('Vexsoluciones: ' . $message);
    }
}
