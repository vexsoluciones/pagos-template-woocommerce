jQuery(document).ready(function($) {
    $('#plugin-update-button').click(function(e) {
        e.preventDefault();
        const url = pluginData.adminUrl;
        $.ajax({
            url: ajaxurl,
            type: 'POST',
            data: {
                action: 'plugin_update'
            },
            success: function(response) {
                window.location.href = url;
            },
            error: function() {
                // Error en la actualización
            }
        });
    });
});